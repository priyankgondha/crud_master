import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from './components/userApp/user-list/user-list.component';
import { UserRegisterComponent } from './components/userApp/user-register/user-register.component';
import { UserUpdateComponent } from './components/userApp/user-update/user-update.component';
import { DemoComponent } from './demo/demo.component';
import { HeaderComponent } from './header/header.component';

const routes: Routes = [
  { path: 'userlist', component: UserListComponent },
  { path: 'adduser', component: UserRegisterComponent },
  { path: '', component: HeaderComponent },
  { path: 'demo', component: DemoComponent },
  { path: 'updateUser/:id', component: UserUpdateComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
