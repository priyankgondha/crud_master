import { createAction } from "@ngrx/store";
import { Action} from "@ngrx/store";

export const show = createAction('[Demo Component] show')
export const displayOrignal = createAction('[Demo Component] display')
export const reset  = createAction('[Demo Component] reset')

export const dataAdd = createAction('[Demo Component] dataAdd')
export const dataRemove = createAction('[Demo component] dataRemove')
// export const dataRemove = '[Demo component] dataRemove'

// export class removeData implements Action {
//   readonly type=dataRemove
//   constructor(public payload:number){}
// }

// export type actions= removeData
