import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from "@ngrx/store"
import { show,displayOrignal,reset,dataAdd ,dataRemove } from '../data.actions';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {


  name: Observable<string>
  dataArr:Observable<string[]>

  ngOnInit(): void {
  }

  constructor(private store: Store<{ data: string,arrDataPipe:Array<string> }> ) {
    this.name = store.pipe(select('data')),
    this.dataArr = store.pipe(select('arrDataPipe'))
  }

  show() {
    this.store.dispatch(show())
  }

  diplayOrignal(){
    this.store.dispatch(displayOrignal())
  }

  reset(){
    this.store.dispatch(reset())
  }

  dataAdd(){
    this.store.dispatch(dataAdd())
  }

  dataRemove(){
    this.store.dispatch(dataRemove());
  }


}
