import { Component, OnInit } from '@angular/core';
import { userObj } from 'src/interface/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  userList: userObj[];

  constructor() {
    this.userList = [];
  }

  ngOnInit(): void {
    const records = localStorage.getItem('userList');
    if (records !== null) {
      this.userList = JSON.parse(records);
    }
  }

  delete(id: any) {
    const oldrecords = localStorage.getItem('userList');
    if (oldrecords !== null) {
      const userList = JSON.parse(oldrecords);
      userList.splice(userList.findIndex((a: any) => a.userId == id), 1)
      localStorage.setItem('userList', JSON.stringify(userList));
      console.log("Ho");
    }
    const records = localStorage.getItem('userList');
    if (records !== null) {
      this.userList = JSON.parse(records);
    }
  }

}
