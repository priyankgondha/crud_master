import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserListComponent } from './components/userApp/user-list/user-list.component';
import { UserRegisterComponent } from './components/userApp/user-register/user-register.component';
import { UserUpdateComponent } from './components/userApp/user-update/user-update.component';
import { HeaderComponent } from './header/header.component';
import { DemoComponent } from './demo/demo.component';
import { StoreModule } from '@ngrx/store';
import { updateName,arrData } from './data.reducer';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserRegisterComponent,
    UserUpdateComponent,
    HeaderComponent,
    DemoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot({ data: updateName ,arrDataPipe:arrData})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
